// write the correct arrow function here
var my_function = (some_array) => { 
    return some_array.map(field => {
        return (field % 2 === 0)? ++field : --field;
    });
};