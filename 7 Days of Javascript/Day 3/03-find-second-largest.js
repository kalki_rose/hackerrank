function processData(myArray) {
    var sortedArray = myArray.sort(function (a, b) {
        return parseInt(a) > parseInt(b);
    });
    
    var largest = sortedArray[sortedArray.length - 1];

    for (var i = sortedArray.length - 1; i >= 0; i--) {
        if (sortedArray[i] < largest) {
            console.log(sortedArray[i]);
            break;
        }
    }
}


// tail starts here
process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input.split('\n')[1].split(' ').map(Number));
});
