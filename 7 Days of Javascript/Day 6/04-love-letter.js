function processRow(row) {
    var count = 0;
    var length = row.length;
    var mid = Math.floor(length / 2);
    
    for (var i = 0; i < mid; i++) {
        if (row.charCodeAt(i) === row.charCodeAt(length - 1 - i)) {
            continue;
        } else {
            var left = row.charCodeAt(i);
            var right = row.charCodeAt(length - 1 - i);
            
            count += (Math.abs(left - right));
        }
    }
    
    return count;
    
}

function processData(input) {
    //Enter your code here
    var parts = input.split("\n");
    
    for (var i = 1; i < parts.length; i++) {
        var count = processRow(parts[i]);
        console.log(count);
    }
} 

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});
