function processData(input) {
    //Enter your code here
    var parts = input.split("\n");
    var max = parseInt(parts[0].split(" ")[1]);
    
    var toyPrices = parts[1].split(" ").sort( (a, b) => {
        return a - b;
    });
    
    var count = 0; 
    for (var i of toyPrices) {
        if (i <= max) {
            ++count;
            max -= i;
        } else {
            break;
        }
    }
    
    console.log(count);  
} 

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});
