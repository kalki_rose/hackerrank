function checkPoint(x, y, matrix) {
    var valid = false;

    if ( matrix[x] != undefined && matrix[x][y] != undefined ) {
        var temp = matrix[x][y];
        
        if (temp.val == 1 && temp.used === false) {
            valid = true;
        }
    }

    return valid;
}

function findRegion(point, matrix) {
    var region = [];
    
    if (checkPoint(point.x, point.y, matrix)) {
        region.push(point);
        point.used = true;
        matrix[point.x][point.y].used = true;

        var potentialPoints = [
            [point.x - 1, point.y - 1],
            [point.x - 1, point.y],
            [point.x - 1, point.y + 1],
            [point.x, point.y - 1],
            [point.x, point.y + 1],
            [point.x + 1, point.y - 1],
            [point.x + 1, point.y],
            [point.x + 1, point.y + 1]
        ];

        var pointsToExpand = [];

        for (var coords of potentialPoints) {
            if (checkPoint(coords[0], coords[1], matrix)) {
                pointsToExpand.push(matrix[coords[0]][coords[1]]);
            }
        }

        for (var i = 0; i < pointsToExpand.length; i++) {
            region = region.concat(findRegion(pointsToExpand[i], matrix));
        }
    }
    return region;
}

function processData(input) {
    //Enter your code here
    'use strict';
    
    var parts = input.split("\n");
    
    var matrix = [];
    
    for (let i = 0; i < parts[0]; i++ ) {
        var row = [];
        var rowParts = parts[i + 2].split(" "); 
        for (var j = 0; j < rowParts.length; j++) {
            var point = {
                val: rowParts[j],
                x: i,
                y: j,
                used: false
            };
            
            row.push(point);
        }
        matrix[i] = row;      
    }
    
    var regions = [];

    for (var row = 0; row < matrix.length; row++) {
        
        for ( var point of matrix[row] ) {
            if (point.val == 1 && point.used === false) {
                var region = findRegion(point, matrix);
                regions.push(region);
                break;
            }
        }
    }
    
    regions.sort((a,b) => { return b.length - a.length});
    
    console.log(regions[0].length);

} 

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});
